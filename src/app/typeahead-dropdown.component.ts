import {
  Component,
  Directive,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ContentChild,
  ElementRef,
  TemplateRef,
  forwardRef,
} from "@angular/core";
import {
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  Validator,
  ValidationErrors,
  FormControl,
  AbstractControl,
  NgModel,
  ControlContainer,
  NgForm,
} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { Observable, fromEvent, empty, of } from "rxjs";
import { map, filter, tap, switchMap, share } from "rxjs/operators";

import { BaseControlValueAccessor } from "./base-control-value-accessor.component";

@Directive({
  selector: "[customDropdownTemplate]",
})
export class CustomDropdownTemplateDirective {}

@Component({
  selector: "dropdown-typeahead-component",
  template: `
    <ng-container *ngIf="dropdownObjsVm">
      <label>{{ label }}</label>
      <ng-container *ngIf="!isReadOnly; else readOnlyText">
        <div class="twrapper">
          <div class="input-filter-box">
            <input
              #filter
              type="text"
              placeholder="Select"
              class="form-control custom-control"
              [attr.title]="
                selectedType && selectedType[activeFlagPropertyName] == false
                  ? 'This is an inactive type.'
                  : null
              "
              [ngClass]="{
                'inactive-type-text':
                  selectedType && selectedType[activeFlagPropertyName] == false
              }"
              (focus)="showDDContent()"
              (blur)="hideDDContent()"
              (keydown.enter)="$event.preventDefault()"
              (keydown.tab)="tabKey()"
              [(ngModel)]="modelText"
              [ngModelOptions]="{ updateOn: 'blur' }"
              [style.font-size]="fontSize"
              #filterNgModel="ngModel"
              typeaheadInvalidInputValidator
              name="ddta-{{ name }}"
            />
            <div
              class="ddIcon"
              [ngClass]="{ 'ddIcon-active': showDD }"
              (mousedown)="toggleDDContent()"
            >
              <span class="caret"></span>
            </div>
          </div>
          <div *ngIf="showDD" class="ddContent">
            <ul #ddItems>
              <ng-container
                *ngIf="
                  !!dropdownObjsVm && dropdownObjsVm.length > 0;
                  else noResult
                "
              >
                <li
                  *ngFor="let item of dropdownObjsVm; let i = index"
                  (mousedown)="onSelectedType(item)"
                  [ngClass]="{
                    'key-focus': i == currentKeyFocus,
                    'inactive-type': item[activeFlagPropertyName] == false
                  }"
                  [attr.title]="
                    !item[activeFlagPropertyName] ? inactiveText(item) : null
                  "
                >
                  <ng-container
                    *ngTemplateOutlet="
                      customDropdownTemplate
                        ? customDropdownTemplate
                        : defaultTemplate;
                      context: { $implicit: item }
                    "
                  >
                  </ng-container>
                  <ng-template #defaultTemplate>
                    {{ item[textField] }}
                  </ng-template>
                </li>
              </ng-container>
              <ng-template #noResult>
                <li>Not found.</li>
              </ng-template>
            </ul>
          </div>
          <div
            *ngIf="dropdownTypeaheadInvalidInputStatus"
            class="alert alert-danger well-sm ddta-invalid-warning"
            [style.font-size]="'0.9em'"
          >
            Please select a valid {{ label }}.
          </div>
        </div>
      </ng-container>
      <ng-template #readOnlyText>
        <div
          class="form-control bkg-readonly"
          [style.font-size]="fontSize"
          [attr.title]="
            selectedType && selectedType[activeFlagPropertyName] == false
              ? 'This is an inactive type.'
              : null
          "
        >
          {{ modelText }}
        </div>
      </ng-template>
    </ng-container>
  `,
  styles: [
    "div.twrapper{display:flex;flex-direction:column;margin-bottom:15px}div.ddContent{position:relative}ul{list-style:none;max-height:40vh;overflow:auto;padding:5px 0;margin:2px 0 0;text-align:left;position:absolute;width:100%;background:#fff;border:1px solid rgba(0,0,0,.15);border-radius:4px;box-shadow:0 6px 12px rgba(0,0,0,.175);z-index:3}ul > li{padding-top:3px;padding-bottom:3px;padding-left:10px;font-size:.9em}ul > li:hover{background-color:#f5f5f5;cursor:pointer}.key-focus{background-color:#f5f5f5}.input-filter-box{position:relative}.input-filter-box > input.inactive-type-text{background-color:#eee8aa}.ddIcon{position:absolute;border:1px solid #ccc;border-radius:0 4px 4px 0;top:0;right:0;width:40px;height:100%;background-color:#e5e4e4;cursor:pointer}.ddIcon:hover,.ddIcon-active{border-color:#8c8c8c;background-color:#d4d4d4}.ddIcon-active > span.caret{transform:rotate(180deg)}.ddIcon > span.caret{position:absolute;top:43%;left:34%}.ddta-invalid-warning{margin-top:15px}li.inactive-type{background-color:#eee8aa}li.inactive-type:hover{background-color:#fabf1a!important}div.bkg-readonly{background-color:#f9f9f9;height:auto;min-height:2.5em}",
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TypeaheadDropdownComponent),
      multi: true,
    },
  ],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})

/**********************************************
* Example:
*
* 
<dropdown-typeahead-component [label]="'Job Code'" [dataSource]="jobCodeTypes" [primaryKeyField]="'JobCode1'"
                                [textField]="'JobCodeTitle'" (onSelect)="onSelectJobCode($event)" name="JobCode"
                                [(ngModel)]="jobCodeEligibilityModel.JobCode" [missingTypeApiUrl]="'api/JobCodeType/{id}'"
                                [activeFlagPropertyName]="'ActiveFlag'" [isReadOnly]="isReadOnly || jobCodeId" required>
<!--The ng-template is optional. Use this is if you want to design the look of each dropdown item.-->
              <ng-template customDropdownTemplate let-item>
                          <div>
                          <h1>{{item.JobCode1}}</h1>
                          <p>Custom html for each dropdown item goes within this ng-template</p>
                          </div>
              </ng-template>
</dropdown-typeahead-component>

*The following input parameters are required
label, primaryKeyField, textField, name, missingTypeApiUrl, activeFlagPropertyName, ngModel
*
*********************************************/
export class TypeaheadDropdownComponent
  extends BaseControlValueAccessor
  implements AfterViewInit
{
  @Input() public label: string;
  @Input() public isReadOnly: boolean = false;
  @Input() public primaryKeyField: string;
  @Input() public fontSize: string;
  @Input() public textField: string; //property name of datasource object to display in dropdown
  @Input() public required: any = false;
  @Input() public name: string; //name attribute for html element. Required for validation to work correctly
  @Input() public missingTypeApiUrl: string; //if passed in ngModel key is not present in the dataSource array, it will get it from this url
  @Input() public activeFlagPropertyName: string; //name of active flag property of dropdown type item...usually its ActiveFlag

  @Input() public sort: boolean = true;
  @Input() public sortField: string;
  @Input() public sortOrder: string = "ASC";

  @Output() onSelect = new EventEmitter();

  @ViewChild("filter", { static: false }) public filterElement: ElementRef;
  @ViewChild("filterNgModel", { static: false })
  public inputFilterHtmlNgModel: NgModel;

  @ContentChild(CustomDropdownTemplateDirective, {
    read: TemplateRef,
    static: false,
  })
  public customDropdownTemplate: TemplateRef<any>;

  public modelKey: any;
  public dropdownObjsVm: Array<any>;
  public showDD: boolean = false;
  public selectedType: any;
  public currentKeyFocus: number = 0; //used to identify which item in dropdown is active when using down/up keys

  //#region getters/setters
  private _dataSource: any[];
  @Input() public set dataSource(value: any[]) {
    if (undefined !== value) {
      this._dataSource = this.sortList(value);
    } else {
      this._dataSource = value;
    }

    this.dropdownObjsVm = [...this._dataSource];
  }

  public get dataSource(): any[] {
    return this._dataSource.sort((a, b) =>
      a[this.textField].localeCompare(b[this.textField])
    );
  }

  private _modelText: string;
  public get modelText(): string {
    if (this.modelKey && this.dataSource) {
      this.selectedType = this.dataSource.find(
        (x) => x[this.primaryKeyField] == this.modelKey
      );

      if (this.selectedType) {
        this._modelText = this.selectedType[this.textField];
      } else {
        //make api request to get missing type
        if (!this.missingTypeApiUrl)
          throw new Error(
            "Inactive/Missing dropdown type api url is required."
          );

        this.missingTypeApiUrl = this.missingTypeApiUrl.replace(
          "{id}",
          this.modelKey
        );

        this.getMissingType(this.missingTypeApiUrl).subscribe(
          (missingType) => {
            if (
              !this.dataSource.some(
                (x) => x[this.primaryKeyField] == this.modelKey
              )
            ) {
              this.dataSource.push(missingType);
              this._modelText = missingType[this.textField];
              this.selectedType = missingType;
              this.dropdownObjsVm = [...this.dataSource];
              this.propagateChange(this.modelKey);
            }
          },
          (error) => console.log(error)
        );
      }
    } else if (!this.modelKey && !this.selectedType) {
      this._modelText = "";
    }

    return this._modelText;
  }

  public set modelText(value: string) {
    this._modelText = value;

    this.setModelState();
  }

  public get dropdownTypeaheadInvalidInputStatus(): boolean {
    let flag: boolean = false;
    if (this.inputFilterHtmlNgModel && this.inputFilterHtmlNgModel.errors)
      return this.inputFilterHtmlNgModel.errors.invalidInputValue;

    return flag;
  }

  public get isValidType() {
    let validType: boolean = false;
    if (this.selectedType && this.selectedType[this.textField])
      validType = this.dataSource.some(
        (x) => x[this.textField] == this.selectedType[this.textField]
      );

    return validType;
  }

  private setModelState(): void {
    this.modelKey = null;
    this.selectedType = null;

    let typedItem = this.dataSource.find(
      (x) =>
        x[this.textField].toLowerCase() ==
        this.filterElement.nativeElement.value.toLowerCase()
    );

    if (typedItem) {
      this.selectedType = typedItem;
      this.modelKey = typedItem[this.primaryKeyField];
    }
  }

  public inactiveText(item: any): string {
    return `${item[this.textField]} is an inactive ${this.label} type.`;
  }

  //done to prevent duplicate api requests
  private _missingTypeObs: Observable<any>;
  private getMissingType(apiUrl: string): Observable<any> {
    if (!this._missingTypeObs) {
      this._missingTypeObs = this.httpService.get(apiUrl).pipe(share());
    }

    return this._missingTypeObs;
  }

  //#endregion

  constructor(private httpService: HttpClient) {
    super();
  }

  ngAfterViewInit() {
    if (this.filterElement) {
      fromEvent(this.filterElement.nativeElement, "keyup")
        .pipe(
          switchMap((event) => {
            let keyEvent: KeyboardEvent = <KeyboardEvent>event;

            if (
              [38, 40, 13].some((x) => x == keyEvent.keyCode) ||
              ((keyEvent.keyCode == 8 || keyEvent.keyCode == 46) &&
                this.filterElement.nativeElement.value.length == 0)
            ) {
              if (
                keyEvent.keyCode == 38 &&
                this.currentKeyFocus >= 1 &&
                this.currentKeyFocus <= this.dropdownObjsVm.length - 1
              ) {
                //up key
                this.currentKeyFocus--;
                this.selectedType = this.dropdownObjsVm[this.currentKeyFocus];
                document
                  ?.getElementsByClassName("key-focus")
                  ?.item(0)
                  ?.scrollIntoView({ behavior: "auto", block: "center" });

                this.modelKey = this.selectedType[this.primaryKeyField];
              } else if (
                keyEvent.keyCode == 40 &&
                this.currentKeyFocus >= 0 &&
                this.currentKeyFocus < this.dropdownObjsVm.length - 1
              ) {
                //down key
                this.currentKeyFocus++;
                this.selectedType = this.dropdownObjsVm[this.currentKeyFocus];
                document
                  ?.getElementsByClassName("key-focus")
                  ?.item(0)
                  ?.scrollIntoView({ behavior: "auto", block: "center" });

                this.modelKey = this.selectedType[this.primaryKeyField];
              } else if (
                keyEvent.keyCode == 13 &&
                ((this.currentKeyFocus >= 0 &&
                  this.currentKeyFocus < this.dropdownObjsVm.length - 1) ||
                  this.dropdownObjsVm.length == 1)
              ) {
                //enter key
                this.onSelectedType(this.dropdownObjsVm[this.currentKeyFocus]);
                this.modelKey = this.selectedType[this.primaryKeyField];
              } else if (keyEvent.keyCode == 8 || keyEvent.keyCode == 46) {
                //backspace or delete
                this.dropdownObjsVm = [...this.dataSource];

                if (!this.showDD) {
                  this.showDD = true;
                }
              }

              return empty();
            } else {
              return of(event).pipe(
                map((event: any) => (<HTMLInputElement>event.target)?.value),
                tap((value: string) => {
                  this.currentKeyFocus = 0;
                  this.dropdownObjsVm = this.dataSource.filter((x) =>
                    x[this.textField]
                      .toLowerCase()
                      .includes(value.toLowerCase())
                  );
                })
              );
            }
          })
        )
        .subscribe((value: any) => {});
    }
  }

  writeValue(obj: any): void {
    if (this.modelKey !== obj) {
      this.modelKey = obj;

      this.selectedType = null;

      if (this.dataSource && this.modelKey)
        this.selectedType = this.dataSource.find(
          (x) => x[this.primaryKeyField] == this.modelKey
        );
    }
  }

  public toggleDDContent(): void {
    if (this.showDD == false) {
      this.showDDContent();

      if (
        this.filterElement.nativeElement !== document.activeElement &&
        this.showDD
      ) {
        this.filterElement.nativeElement.focus();
        event?.preventDefault();
      }
    } else {
      this.hideDDContent();
    }
  }

  public showDDContent(): void {
    if (this.showDD == false) {
      this.showDD = true;
    }
  }

  public hideDDContent(): void {
    if (this.showDD == true) {
      this.showDD = false;
      this.dropdownObjsVm = [...this.dataSource];
      this.currentKeyFocus = 0;

      this.setModelState();

      this.propagateChange(this.modelKey);
    }
  }

  public onSelectedType(type: any): void {
    this.selectedType = type;
    this.modelKey = this.selectedType[this.primaryKeyField];
    this.propagateChange(this.modelKey);
    this.onSelect.emit(this.selectedType);

    this.showDD = false;
    this.dropdownObjsVm = [...this.dataSource];
    this.currentKeyFocus = 0;
  }

  public tabKey(): void {
    if (this.dropdownObjsVm && this.dropdownObjsVm.length >= 1) {
      event?.preventDefault();
      this.onSelectedType(this.dropdownObjsVm[this.currentKeyFocus]);
      this.modelKey = this.selectedType[this.primaryKeyField];
    }
  }

  private sortList(arr: any[]): any[] {
    if (true === this.sort) {
      if (undefined === this.sortField) {
        this.sortField = this.textField;
      }
      let orderReverser: number =
        this.sortOrder.toLowerCase() === "desc" ? -1 : 1;

      return arr.sort(
        (a, b) =>
          (a[this.sortField] > b[this.sortField] ? 1 : -1) * orderReverser
      );
    } else {
      return arr;
    }
  }
}

// @Directive({
//   selector: "dropdown-typeahead-component[required][ngModel]",
//   providers: [
//     {
//       provide: NG_VALIDATORS,
//       useExisting: DropdownTypeaheadRequiredDirective,
//       multi: true,
//     },
//   ],
// })
// export class DropdownTypeaheadRequiredDirective implements Validator {
//   private parentComponent: TypeaheadDropdownComponent;

//   constructor(private prtComponent: TypeaheadDropdownComponent) {
//     this.parentComponent = prtComponent;
//   }

//   validate(c: FormControl): { [key: string]: any } {
//     if (this.parentComponent.required || this.parentComponent.required === "") {
//       if (
//         c.value !== null &&
//         this.parentComponent.isValidType &&
//         this.parentComponent.selectedType
//       ) {
//         return null;
//       } else {
//         return {
//           required: {
//             valid: false,
//           },
//         };
//       }
//     }
//   }
// }

// ///Checks if user entered value is a valid type from dropdown; validation does not apply to empty typeaheads (user did not enter anything and is not a required field)
// @Directive({
//   selector:
//     "[typeaheadInvalidInputValidator]formControlName],[typeaheadInvalidInputValidator][formControl],[typeaheadInvalidInputValidator][ngModel]",
//   providers: [
//     {
//       provide: NG_VALIDATORS,
//       useExisting: DropdownTypeaheadInvalidInputValidatorDirective,
//       multi: true,
//     },
//   ],
// })
// export class DropdownTypeaheadInvalidInputValidatorDirective {
//   private parentComponent: TypeaheadDropdownComponent;

//   constructor(private prtComponent: TypeaheadDropdownComponent) {
//     this.parentComponent = prtComponent;
//   }

//   validate(control: AbstractControl): ValidationErrors | null {
//     if (
//       control &&
//       control.value &&
//       control.value.length > 0 &&
//       !this.parentComponent.isValidType
//     ) {
//       return {
//         invalidInputValue: true,
//       };
//     } else return null;
//   }
// }

///TODO
//1. determine how not call 2nd blur method which calls setModelState after ngmodel does the same thing setModelState
