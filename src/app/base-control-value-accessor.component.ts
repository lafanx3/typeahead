import { ControlValueAccessor } from '@angular/forms';

export abstract class BaseControlValueAccessor implements ControlValueAccessor {

protected propagateChange: Function = () => { };
protected propagateTouch: Function = () => { };

public onTouch(): void {
this.propagateTouch();
}

writeValue(obj: any): void {

}

registerOnChange(fn: any): void {
this.propagateChange = fn;
}

registerOnTouched(fn: any): void {
this.propagateTouch = fn;
}
}
