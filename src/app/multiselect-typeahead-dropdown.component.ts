import {
  Component,
  Input,
  Output,
  EventEmitter,
  forwardRef,
  ViewChild,
  ElementRef,
  IterableDiffers,
} from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";

import { BaseControlValueAccessor } from "./base-control-value-accessor.component";

declare var $: any;

@Component({
  selector: "multiselect-typeahead-dropdown-component",
  template: `
    <div class="form-group">
      <label *ngIf="label">{{ label }}</label
      ><br />
      <ng-container *ngIf="!isReadOnly; else readOnlyText">
        <div class="dropdown custom-control" #multi>
          <div
            class="input-group dropdown-toggle"
            click-outside
            (onClickOutside)="onTouch()"
            data-toggle="dropdown"
          >
            <input
              type="text"
              class="form-control"
              [ngModel]="searchText"
              (ngModelChange)="onSearchModelChange($event)"
            />
            <div class="input-group-btn">
              <button
                type="button"
                class="btn btn-default"
                [style.font-size]="fontSize"
              >
                <span class="caret"></span>
              </button>
            </div>
          </div>
          <ul
            class="dropdown-menu dropdown-menu-right pre-scrollable"
            [style.font-size]="fontSize"
          >
            <li *ngFor="let item of items">
              <span class="item" [hidden]="filterDropdownItem(item[textField])">
                <input
                  type="checkbox"
                  [checked]="isCheckedItem(item)"
                  (change)="itemSelected(item)"
                />
                {{ item[textField] }}
              </span>
            </li>
          </ul>
        </div>
      </ng-container>
      <ng-template #readOnlyText>
        <div class="form-control pre-scrollable bkg-readonly">
          {{ selectedItems && 0 < selectedItems.length ? getDisplayText : " " }}
        </div>
      </ng-template>
    </div>
  `,
  styles: [
    'ul.dropdown-menu{overflow-y:auto;width:25vw;max-height:50vh;padding-left:15px;}input[type="checkbox"]{height:15px;width: 15px;cursor:pointer;margin-right:5px;}button[type="button"].custom-control{white-space:normal;}div.bkg-readonly{background-color: #f9f9f9;}.dropdown-menu{width: 100%;}[hidden]{display:none!important;}',
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiSelectTypeaheadDropdownComponent),
      multi: true,
    },
  ],
})
export class MultiSelectTypeaheadDropdownComponent extends BaseControlValueAccessor {
  @ViewChild("multi", { static: false }) multidropdownElem: ElementRef;

  @Input() public isReadOnly: boolean;

  private _orderedItems: Array<any>;
  @Input() public set items(val: any[]) {
    // Type list to fill dropdown values
    if (undefined !== val) {
      this._orderedItems = this.sortList(val);
    } else {
      this._orderedItems = val;
    }
  }
  public get items(): any[] {
    return this._orderedItems;
  }

  @Input() public label: string;
  @Input() public primaryKeyField: string;
  @Input() public textField: string;
  @Input() public fontSize: string;

  @Input() public sort: boolean = true;
  @Input() public sortField: string;
  @Input() public sortOrder: string = "ASC";

  @Output() public onSelectType = new EventEmitter();

  public pattern: any; // matching regex pattern
  public differ: any;

  public selectedItems: Array<any>; //model

  public _searchText: string;
  public set searchText(val: string) {
    this._searchText = val;
    this.setRegexPattern();
  }
  public get searchText(): string {
    return this._searchText;
  }

  constructor(public differs: IterableDiffers) {
    super();
    this.differ = differs.find([]).create(undefined);
  }

  ngAfterViewInit() {
    if (this.multidropdownElem && this.multidropdownElem.nativeElement) {
      // we need to prevent the dropdown from closing AND set the
      // focus back to the input element when dropdown appears
      $(this.multidropdownElem.nativeElement).on({
        "shown.bs.dropdown": function () {
          this.closable = false;
          $(this).find("input.form-control").focus();
        },
      });
    }
  }

  ngDoCheck() {
    // watch for changes to the selected items array... push, pop, etc.
    const change = this.differ.diff(this.selectedItems);
    if (change) {
      this.searchText = this.getDisplayText;
    }
  }

  public get getDisplayText(): string {
    if (this.selectedItems && this.selectedItems.length > 0) {
      return this.selectedItems.map((x) => x[this.textField]).join(", ");
    } else {
      return "";
    }
  }

  writeValue(obj: any): void {
    if (this.selectedItems !== obj) {
      this.selectedItems = obj;
    }
  }

  public itemSelected(item: any): void {
    let indexOfItem: number = -1;
    if (this.selectedItems) {
      indexOfItem = this.selectedItems.findIndex(
        (x) => x[this.primaryKeyField] == item[this.primaryKeyField]
      );
    } else {
      this.selectedItems = new Array<any>();
    }

    if (indexOfItem == -1) {
      this.selectedItems.push(item);
    } else {
      this.selectedItems.splice(indexOfItem, 1);
    }

    this.propagateChange(this.selectedItems);
    this.onSelectType.emit({ SelectedItems: this.selectedItems });
  }

  public isCheckedItem(item: any): boolean {
    if (
      this.selectedItems.some(
        (x) => x[this.primaryKeyField] == item[this.primaryKeyField]
      )
    ) {
      return true;
    } else {
      return false;
    }
  }

  public setRegexPattern(): void {
    let query: string = "";
    if (this.searchText && -1 < this.searchText.indexOf(",")) {
      let s: string[] = this.searchText.split(",");
      s.forEach((x) => {
        query += x.trim() + "|";
      });
      query = query.substring(0, query.length - 1);
    } else {
      query = this.searchText;
    }
    this.pattern = new RegExp("^" + query, "i");
  }

  public onSearchModelChange(e: any): void {
    this.searchText = e;
  }

  // filters list items based on the search text...
  public filterDropdownItem(txt: string): boolean {
    if (this.searchTextValid) {
      return !this.pattern.test(txt);
    } else {
      return false;
    }
  }

  public get searchTextValid(): boolean {
    return !!this.searchText && this.searchText.length > 1;
  }

  private sortList(arr: any[]): any[] {
    if (true === this.sort) {
      if (undefined === this.sortField) {
        this.sortField = this.textField;
      }
      let orderReverser: number =
        this.sortOrder.toLowerCase() === "desc" ? -1 : 1;

      return arr.sort(
        (a, b) =>
          (a[this.sortField] > b[this.sortField] ? 1 : -1) * orderReverser
      );
    } else {
      return arr;
    }
  }
}
